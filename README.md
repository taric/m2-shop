#Setup Dev Environment

## Install Dependencies ##

Execute composer in order to install the magento core and all 3th party libraries.

```
composer install -d src --ignore-platform-reqs
```

If you want to create new project from scratch
```
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition <installation directory name>
```

## Start Docker Containers ##

Start up all docker containers by running:

### For Mac ###

> Optional: Install Homebrew if not yet installed  
> https://brew.sh/

> Install docker-sync  
> http://docker-sync.io/

```
docker-sync start  
docker-compose -f docker-compose-sync.yml up -d
```

>To stop all containers
```
docker stop $(docker ps -a -q)
```

>To remove all containers
```
docker rm $(docker ps -a -q)
```


### For Linux ###

```
docker-compose -f docker-compose.yml up -d
```

## Prepare Dev Environment ##

Copy the env file containing basic configuration for the dev Environment:

```
cp src/app/etc/env.php.local src/app/etc/env.php
```

In order to open the shop in the browser, the following host entry is needed (for Mac):

```
127.0.0.1   m2-shop.local
``` 

## Connection or command execution

```
docker exec -it <container-name> bash (root user)
docker exec -itu www-data <container-name> bash (www-data user) 


docker exec -u www-data php-m2-shop /var/www/html/bin/magento setup:upgrade
``` 

## Set up a new magento instance
```sh
docker exec -itu www-data php-m2-shop bash
php bin/magento setup:install --backend-frontname="backend" \
--db-engine="mysql" --db-host="mysql-m2-shop" --cleanup-database \
--db-name="m2_shop" --db-user="root" --db-password="root" \
--base-url="https://m2-shop.local/" --base-url-secure="https://m2-shop.local/" \
--use-secure="0" --use-secure-admin="1" --use-rewrites="1" \
--language="en_US" --currency="EUR" --timezone="Europe/Kiev" \
--admin-user="taric" --admin-password="changeme123" \
--admin-email="taras.demianchenko@flagbit.de" --admin-firstname="Taras" --admin-lastname="Demianchenko"
exit
```

## Run setup/upgrade in docker container using alias:
```
$ setup
```

## Aliases for Docker Container ##
```
$ m             # Change current directory to magento installation
$ perm          # Fix Permissions for Magento
$ setup         # Run bin/magento setup:upgrade and rebuild page afterwards
$ rein          # Run bin/magento indexer:reindex
$ cache         # Clean Magento cache (cache, page_cache, generation, ...)
```

## Configure Mailcatcher

Url: http://m2-shop.local:1080/

* Go to Stores -> Configuration -> Advanced -> System
* Click on the Tab Gmail/Google Apps SMTP Pro
    * Set Authentication method to `plain`
    * Set SSL type `none`
    * Set SMTP Host to `mailcatcher-m2-shop`
    * Set SMTP Port to `1025`
    * Set Username to `mailcatcher_user`
    * Set Password to `mailcatcher_pass`